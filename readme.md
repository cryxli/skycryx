# SkyCryx
This project documents the work in progress to create Minecraft SkyBlock modpack based on tech and equivalent exchange.

*SkyCryx* is only a working title.

## Components
- [MultiMC](https://multimc.org) since Curse launcher never worked.
- [ProjectE](https://minecraft.curseforge.com/projects/projecte) providing equivalent exchange.
- [Ender IO](https://minecraft.curseforge.com/projects/ender-io) as the main tech mod.
- [Chickens](https://minecraft.curseforge.com/projects/chickens) for farming resources

## Problems
- SkyBlock will have [Ex Nihilo](https://minecraft.curseforge.com/projects/ex-nihilo-adscensio) in one form or another.
- [Hatchery](https://minecraft.curseforge.com/projects/hatchery) just abandoned [WAILA](https://minecraft.curseforge.com/projects/waila) in the latest version.

## License
TBD, but I tend to GPL since a mod pack contains work of others and requires a lot of tweaking until it works.
