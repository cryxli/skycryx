#Name: projecte.zs
#Author: cryxli

print("Initializing 'ProjectE recipes'...");

// Philisopher's Stone
recipes.remove(<projecte:item.pe_philosophers_stone>);
recipes.addShaped(<projecte:item.pe_philosophers_stone>, [[<minecraft:dirt>, <minecraft:dirt>], [<minecraft:dirt>, <minecraft:dirt>]]);

// Packed Ice
recipes.remove(<minecraft:packed_ice>);
recipes.addShapeless(<minecraft:packed_ice>, [<projecte:item.pe_zero_ring>.reuse(), <minecraft:ice>, <minecraft:ice>, <minecraft:ice>, <minecraft:ice>]);

print("Initialized 'ProjectE recipes'");