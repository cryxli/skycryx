#Name: harvestcraft-saplings.zs
#Author: cryxli

print("Initializing 'HarvestCraft tree recipes'...");

# Almond
recipes.remove(<harvestcraft:almondItem>);
recipes.addShapeless(<harvestcraft:almondItem>, [
    <harvestcraft:oatsItem>, <ore:treeSapling>,
    <projecte:item.pe_philosophers_stone>.reuse()]);

# Apricot
recipes.remove(<harvestcraft:apricotItem>);
recipes.addShapeless(<harvestcraft:apricotItem>, [
    <harvestcraft:artichokeItem>, <ore:treeSapling>,
    <projecte:item.pe_philosophers_stone>.reuse()]);

# Avocado
recipes.remove(<harvestcraft:avocadoItem>);
recipes.addShapeless(<harvestcraft:avocadoItem>, [
    <harvestcraft:sweetpotatoItem>, <ore:treeSapling>,
    <projecte:item.pe_philosophers_stone>.reuse()]);

# Banana
recipes.remove(<harvestcraft:bananaItem>);
recipes.addShapeless(<harvestcraft:bananaItem>, [
    <harvestcraft:kiwiItem>, <ore:treeSapling>,
    <projecte:item.pe_philosophers_stone>.reuse()]);

# Cashew
recipes.remove(<harvestcraft:cashewItem>);
recipes.addShapeless(<harvestcraft:cashewItem>, [
    <harvestcraft:walnutItem>, <ore:treeSapling>,
    <projecte:item.pe_philosophers_stone>.reuse()]);

# Cherry
recipes.remove(<harvestcraft:cherryItem>);
recipes.addShapeless(<harvestcraft:cherryItem>, [
    <harvestcraft:radishItem>, <ore:treeSapling>,
    <projecte:item.pe_philosophers_stone>.reuse()]);

# Chestnut
recipes.remove(<harvestcraft:chestnutItem>);
recipes.addShapeless(<harvestcraft:chestnutItem>, [
    <harvestcraft:durianItem>, <ore:treeSapling>,
    <projecte:item.pe_philosophers_stone>.reuse()]);

# Cinnamon
recipes.remove(<harvestcraft:cinnamonItem>);
recipes.addShapeless(<harvestcraft:cinnamonItem>, [
    <harvestcraft:bambooshootItem>, <ore:treeSapling>,
    <projecte:item.pe_philosophers_stone>.reuse()]);

# Coconut
recipes.remove(<harvestcraft:coconutItem>);
recipes.addShapeless(<harvestcraft:coconutItem>, [
    <harvestcraft:cantaloupeItem>, <ore:treeSapling>,
    <projecte:item.pe_philosophers_stone>.reuse()]);

# Date
recipes.remove(<harvestcraft:dateItem>);
recipes.addShapeless(<harvestcraft:dateItem>, [
    <harvestcraft:raisinsItem>, <ore:treeSapling>,
    <projecte:item.pe_philosophers_stone>.reuse()]);

# Dragonfruit
recipes.remove(<harvestcraft:dragonfruitItem>);
recipes.addShapeless(<harvestcraft:dragonfruitItem>, [
    <harvestcraft:pineappleItem>, <ore:treeSapling>,
    <projecte:item.pe_philosophers_stone>.reuse()]);

# Durian
recipes.remove(<harvestcraft:durianItem>);
recipes.addShapeless(<harvestcraft:durianItem>, [
    <harvestcraft:cashewItem>, <ore:treeSapling>,
    <projecte:item.pe_philosophers_stone>.reuse()]);

# Fig
recipes.remove(<harvestcraft:figItem>);
recipes.addShapeless(<harvestcraft:figItem>, [
    <harvestcraft:strawberryItem>, <ore:treeSapling>,
    <projecte:item.pe_philosophers_stone>.reuse()]);

# Grapefruit
recipes.remove(<harvestcraft:grapefruitItem>);
recipes.addShapeless(<harvestcraft:grapefruitItem>, [
    <harvestcraft:orangeItem>, <ore:treeSapling>,
    <projecte:item.pe_philosophers_stone>.reuse()]);

# Lemon
recipes.remove(<harvestcraft:lemonItem>);
recipes.addShapeless(<harvestcraft:lemonItem>, [
    <harvestcraft:parsnipItem>, <ore:treeSapling>,
    <projecte:item.pe_philosophers_stone>.reuse()]);

# Lime
recipes.remove(<harvestcraft:limeItem>);
recipes.addShapeless(<harvestcraft:limeItem>, [
    <harvestcraft:lemonItem>, <ore:treeSapling>,
    <projecte:item.pe_philosophers_stone>.reuse()]);

# Maple Syrup
recipes.remove(<harvestcraft:maplesyrupItem>);
recipes.addShapeless(<harvestcraft:maplesyrupItem>, [
    <harvestcraft:honeycomb>, <ore:treeSapling>,
    <projecte:item.pe_philosophers_stone>.reuse()]);

# Mango
recipes.remove(<harvestcraft:mangoItem>);
recipes.addShapeless(<harvestcraft:mangoItem>, [
    <harvestcraft:papayaItem>, <ore:treeSapling>,
    <projecte:item.pe_philosophers_stone>.reuse()]);

# Nutmeg
recipes.remove(<harvestcraft:nutmegItem>);
recipes.addShapeless(<harvestcraft:nutmegItem>, [
    <harvestcraft:spiceleafItem>, <ore:treeSapling>,
    <projecte:item.pe_philosophers_stone>.reuse()]);

# Olive
recipes.remove(<harvestcraft:oliveItem>);
recipes.addShapeless(<harvestcraft:oliveItem>, [
    <harvestcraft:scallionItem>, <ore:treeSapling>,
    <projecte:item.pe_philosophers_stone>.reuse()]);

# Orange
recipes.remove(<harvestcraft:orangeItem>);
recipes.addShapeless(<harvestcraft:orangeItem>, [
    <harvestcraft:bellpepperItem>, <ore:treeSapling>,
    <projecte:item.pe_philosophers_stone>.reuse()]);

# Papaya
recipes.remove(<harvestcraft:papayaItem>);
recipes.addShapeless(<harvestcraft:papayaItem>, [
    <harvestcraft:eggplantItem>, <ore:treeSapling>,
    <projecte:item.pe_philosophers_stone>.reuse()]);

# Peach
recipes.remove(<harvestcraft:peachItem>);
recipes.addShapeless(<harvestcraft:peachItem>, [
    <harvestcraft:apricotItem>, <ore:treeSapling>,
    <projecte:item.pe_philosophers_stone>.reuse()]);

# Pear
recipes.remove(<harvestcraft:pearItem>);
recipes.addShapeless(<harvestcraft:pearItem>, [
    <harvestcraft:cucumberItem>, <ore:treeSapling>,
    <projecte:item.pe_philosophers_stone>.reuse()]);

# Pecan
recipes.remove(<harvestcraft:pecanItem>);
recipes.addShapeless(<harvestcraft:pecanItem>, [
    <harvestcraft:almondItem>, <ore:treeSapling>,
    <projecte:item.pe_philosophers_stone>.reuse()]);

# Peppercorn
recipes.remove(<harvestcraft:peppercornItem>);
recipes.addShapeless(<harvestcraft:peppercornItem>, [
    <harvestcraft:chilipepperItem>, <ore:treeSapling>,
    <projecte:item.pe_philosophers_stone>.reuse()]);

# Persimmon
recipes.remove(<harvestcraft:persimmonItem>);
recipes.addShapeless(<harvestcraft:persimmonItem>, [
    <harvestcraft:tomatoItem>, <ore:treeSapling>,
    <projecte:item.pe_philosophers_stone>.reuse()]);

# Pistachio
recipes.remove(<harvestcraft:pistachioItem>);
recipes.addShapeless(<harvestcraft:pistachioItem>, [
    <harvestcraft:peanutItem>, <ore:treeSapling>,
    <projecte:item.pe_philosophers_stone>.reuse()]);

# Plum
recipes.remove(<harvestcraft:plumItem>);
recipes.addShapeless(<harvestcraft:plumItem>, [
    <harvestcraft:turnipItem>, <ore:treeSapling>,
    <projecte:item.pe_philosophers_stone>.reuse()]);

# Pomegranate
recipes.remove(<harvestcraft:pomegranateItem>);
recipes.addShapeless(<harvestcraft:pomegranateItem>, [
    <minecraft:apple>, <ore:treeSapling>,
    <projecte:item.pe_philosophers_stone>.reuse()]);

# Starfruit
recipes.remove(<harvestcraft:starfruitItem>);
recipes.addShapeless(<harvestcraft:starfruitItem>, [
    <harvestcraft:okraItem>, <ore:treeSapling>,
    <projecte:item.pe_philosophers_stone>.reuse()]);

# Vanilla Bean
recipes.remove(<harvestcraft:vanillabeanItem>);
recipes.addShapeless(<harvestcraft:vanillabeanItem>, [
    <harvestcraft:tealeafItem>, <ore:treeSapling>,
    <projecte:item.pe_philosophers_stone>.reuse()]);

# Walnut
recipes.remove(<harvestcraft:walnutItem>);
recipes.addShapeless(<harvestcraft:walnutItem>, [
    <harvestcraft:waterchestnutItem>, <ore:treeSapling>,
    <projecte:item.pe_philosophers_stone>.reuse()]);

# Gooseberry
recipes.remove(<harvestcraft:gooseberryItem>);
recipes.addShapeless(<harvestcraft:gooseberryItem>, [
    <harvestcraft:blueberryItem>, <ore:treeSapling>,
    <projecte:item.pe_philosophers_stone>.reuse()]);


print("Initialized 'HarvestCraft tree recipes'");