#Name: harvestcraft.zs
#Author: cryxli

print("Initializing 'HarvestCraft plant recipes'...");

// Barley
recipes.remove(<harvestcraft:barleyItem>);
recipes.addShapeless(<harvestcraft:barleyItem>, [
    <minecraft:wheat>, <minecraft:wheat>,
    <minecraft:wheat>, <minecraft:wheat>,
    <minecraft:reeds>, <minecraft:reeds>,
    <minecraft:reeds>, <minecraft:reeds>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:barleyseedItem>);

// Rye
recipes.remove(<harvestcraft:ryeItem>);
recipes.addShapeless(<harvestcraft:ryeItem>, [
    <minecraft:wheat>, <minecraft:wheat>,
    <minecraft:wheat>, <minecraft:wheat>,
    <harvestcraft:barleyItem>, <harvestcraft:barleyItem>,
    <harvestcraft:barleyItem>, <harvestcraft:barleyItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:ryeseedItem>);

// Corn
recipes.remove(<harvestcraft:cornItem>);
recipes.addShapeless(<harvestcraft:cornItem>, [
    <harvestcraft:ryeItem>, <harvestcraft:ryeItem>,
    <harvestcraft:ryeItem>, <harvestcraft:ryeItem>,
    <harvestcraft:barleyItem>, <harvestcraft:barleyItem>,
    <harvestcraft:barleyItem>, <harvestcraft:barleyItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:cornseedItem>);

// Oats
recipes.remove(<harvestcraft:oatsItem>);
recipes.addShapeless(<harvestcraft:oatsItem>, [
    <harvestcraft:barleyItem>, <harvestcraft:barleyItem>,
    <harvestcraft:barleyItem>, <harvestcraft:barleyItem>,
    <minecraft:reeds>, <minecraft:reeds>,
    <minecraft:reeds>, <minecraft:reeds>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:oatsseedItem>);

// Sweet Potato
recipes.remove(<harvestcraft:sweetpotatoItem>);
recipes.addShapeless(<harvestcraft:sweetpotatoItem>, [
    <minecraft:reeds>, <minecraft:reeds>,
    <minecraft:reeds>, <minecraft:reeds>,
    <minecraft:potato>, <minecraft:potato>,
    <minecraft:potato>, <minecraft:potato>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:sweetpotatoseedItem>);

// Beans
recipes.remove(<harvestcraft:beanItem>);
recipes.addShapeless(<harvestcraft:beanItem>, [
    <minecraft:pumpkin>, <minecraft:pumpkin>,
    <minecraft:pumpkin>, <minecraft:pumpkin>,
    <minecraft:potato>, <minecraft:potato>,
    <minecraft:potato>, <minecraft:potato>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:beanseedItem>);

// Tomato
recipes.remove(<harvestcraft:tomatoItem>);
recipes.addShapeless(<harvestcraft:tomatoItem>, [
    <harvestcraft:sweetpotatoItem>, <harvestcraft:sweetpotatoItem>,
    <harvestcraft:sweetpotatoItem>, <harvestcraft:sweetpotatoItem>,
    <minecraft:carrot>, <minecraft:carrot>,
    <minecraft:carrot>, <minecraft:carrot>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:tomatoseedItem>);

// Rice
recipes.remove(<harvestcraft:riceItem>);
recipes.addShapeless(<harvestcraft:riceItem>, [
    <harvestcraft:ryeItem>, <harvestcraft:ryeItem>,
    <harvestcraft:ryeItem>, <harvestcraft:ryeItem>,
    <minecraft:reeds>, <minecraft:reeds>,
    <minecraft:reeds>, <minecraft:reeds>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:riceseedItem>);

// Soybean
recipes.remove(<harvestcraft:soybeanItem>);
recipes.addShapeless(<harvestcraft:soybeanItem>, [
    <harvestcraft:riceItem>, <harvestcraft:riceItem>,
    <harvestcraft:riceItem>, <harvestcraft:riceItem>,
    <harvestcraft:beanItem>, <harvestcraft:beanItem>,
    <harvestcraft:beanItem>, <harvestcraft:beanItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:soybeanseedItem>);

// Blue Orchid
recipes.remove(<minecraft:red_flower:1>);
recipes.addShapeless(<minecraft:red_flower:1>, [
    <minecraft:red_flower>, <minecraft:red_flower>,
    <minecraft:red_flower>, <minecraft:red_flower>,
    <minecraft:yellow_flower>, <minecraft:yellow_flower>,
    <minecraft:yellow_flower>, <minecraft:yellow_flower>,
    <projecte:item.pe_philosophers_stone>.reuse()]);

// Daisy
recipes.remove(<minecraft:red_flower:8>);
recipes.addShapeless(<minecraft:red_flower:8>, [
    <minecraft:red_flower:1>, <minecraft:red_flower:1>,
    <minecraft:red_flower:1>, <minecraft:red_flower:1>,
    <minecraft:yellow_flower>, <minecraft:yellow_flower>,
    <minecraft:yellow_flower>, <minecraft:yellow_flower>,
    <projecte:item.pe_philosophers_stone>.reuse()]);

// Allium
recipes.remove(<minecraft:red_flower:2>);
recipes.addShapeless(<minecraft:red_flower:2>, [
    <minecraft:red_flower:1>, <minecraft:red_flower:1>,
    <minecraft:red_flower:1>, <minecraft:red_flower:1>,
    <minecraft:red_flower>, <minecraft:red_flower>,
    <minecraft:red_flower>, <minecraft:red_flower>,
    <projecte:item.pe_philosophers_stone>.reuse()]);

// Celery
recipes.remove(<harvestcraft:celeryItem>);
recipes.addShapeless(<harvestcraft:celeryItem>, [
    <minecraft:red_flower:2>, <minecraft:red_flower:2>,
    <minecraft:red_flower:2>, <minecraft:red_flower:2>,
    <minecraft:wheat>, <minecraft:wheat>,
    <minecraft:wheat>, <minecraft:wheat>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:celeryseedItem>);

// Lettuce
recipes.remove(<harvestcraft:lettuceItem>);
recipes.addShapeless(<harvestcraft:lettuceItem>, [
    <minecraft:red_flower:8>, <minecraft:red_flower:8>,
    <minecraft:red_flower:8>, <minecraft:red_flower:8>,
    <harvestcraft:celeryItem>, <harvestcraft:celeryItem>,
    <harvestcraft:celeryItem>, <harvestcraft:celeryItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:lettuceseedItem>);

// Broccoli
recipes.remove(<harvestcraft:broccoliItem>);
recipes.addShapeless(<harvestcraft:broccoliItem>, [
    <minecraft:red_flower:8>, <minecraft:red_flower:8>,
    <minecraft:red_flower:8>, <minecraft:red_flower:8>,
    <harvestcraft:lettuceItem>, <harvestcraft:lettuceItem>,
    <harvestcraft:lettuceItem>, <harvestcraft:lettuceItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:broccoliseedItem>);

// Cabbage
recipes.remove(<harvestcraft:cabbageItem>);
recipes.addShapeless(<harvestcraft:cabbageItem>, [
    <harvestcraft:broccoliItem>, <harvestcraft:broccoliItem>,
    <harvestcraft:broccoliItem>, <harvestcraft:broccoliItem>,
    <harvestcraft:lettuceItem>, <harvestcraft:lettuceItem>,
    <harvestcraft:lettuceItem>, <harvestcraft:lettuceItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:cabbageseedItem>);

// Scallion
recipes.remove(<harvestcraft:scallionItem>);
recipes.addShapeless(<harvestcraft:scallionItem>, [
    <minecraft:carrot>, <minecraft:carrot>,
    <minecraft:carrot>, <minecraft:carrot>,
    <minecraft:reeds>, <minecraft:reeds>,
    <minecraft:reeds>, <minecraft:reeds>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:scallionseedItem>);

// Leek
recipes.remove(<harvestcraft:leekItem>);
recipes.addShapeless(<harvestcraft:leekItem>, [
    <harvestcraft:scallionItem>, <harvestcraft:scallionItem>,
    <harvestcraft:scallionItem>, <harvestcraft:scallionItem>,
    <harvestcraft:celeryItem>, <harvestcraft:celeryItem>,
    <harvestcraft:celeryItem>, <harvestcraft:celeryItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:leekseedItem>);

// Okra
recipes.remove(<harvestcraft:okraItem>);
recipes.addShapeless(<harvestcraft:okraItem>, [
    <harvestcraft:leekItem>, <harvestcraft:leekItem>,
    <harvestcraft:leekItem>, <harvestcraft:leekItem>,
    <harvestcraft:beanItem>, <harvestcraft:beanItem>,
    <harvestcraft:beanItem>, <harvestcraft:beanItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:okraseedItem>);

// Peas
recipes.remove(<harvestcraft:peasItem>);
recipes.addShapeless(<harvestcraft:peasItem>, [
    <harvestcraft:soybeanItem>, <harvestcraft:soybeanItem>,
    <harvestcraft:soybeanItem>, <harvestcraft:soybeanItem>,
    <harvestcraft:okraItem>, <harvestcraft:okraItem>,
    <harvestcraft:okraItem>, <harvestcraft:okraItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:peasseedItem>);

// Brussel Sprout
recipes.remove(<harvestcraft:brusselsproutItem>);
recipes.addShapeless(<harvestcraft:brusselsproutItem>, [
    <harvestcraft:peasItem>, <harvestcraft:peasItem>,
    <harvestcraft:peasItem>, <harvestcraft:peasItem>,
    <harvestcraft:cabbageItem>, <harvestcraft:cabbageItem>,
    <harvestcraft:cabbageItem>, <harvestcraft:cabbageItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:brusselsproutseedItem>);

// Radish
recipes.remove(<harvestcraft:radishItem>);
recipes.addShapeless(<harvestcraft:radishItem>, [
    <harvestcraft:tomatoItem>, <harvestcraft:tomatoItem>,
    <harvestcraft:tomatoItem>, <harvestcraft:tomatoItem>,
    <harvestcraft:brusselsproutItem>, <harvestcraft:brusselsproutItem>,
    <harvestcraft:brusselsproutItem>, <harvestcraft:brusselsproutItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:radishseedItem>);

// Beet
recipes.remove(<harvestcraft:beetItem>);
recipes.addShapeless(<harvestcraft:beetItem>, [
    <harvestcraft:radishItem>, <harvestcraft:radishItem>,
    <harvestcraft:radishItem>, <harvestcraft:radishItem>,
    <minecraft:carrot>, <minecraft:carrot>,
    <minecraft:carrot>, <minecraft:carrot>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:beetseedItem>);

// Strawberry
recipes.remove(<harvestcraft:strawberryItem>);
recipes.addShapeless(<harvestcraft:strawberryItem>, [
    <harvestcraft:radishItem>, <harvestcraft:radishItem>,
    <harvestcraft:radishItem>, <harvestcraft:radishItem>,
    <minecraft:red_flower>, <minecraft:red_flower>,
    <minecraft:red_flower>, <minecraft:red_flower>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:strawberryseedItem>);

// Cantaloupe
recipes.remove(<harvestcraft:cantaloupeItem>);
recipes.addShapeless(<harvestcraft:cantaloupeItem>, [
    <harvestcraft:strawberryItem>, <harvestcraft:strawberryItem>,
    <harvestcraft:strawberryItem>, <harvestcraft:strawberryItem>,
    <minecraft:melon>, <minecraft:melon>,
    <minecraft:melon>, <minecraft:melon>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:cantaloupeseedItem>);

// Kiwi
recipes.remove(<harvestcraft:kiwiItem>);
recipes.addShapeless(<harvestcraft:kiwiItem>, [
    <harvestcraft:strawberryItem>, <harvestcraft:strawberryItem>,
    <harvestcraft:strawberryItem>, <harvestcraft:strawberryItem>,
    <harvestcraft:cantaloupeItem>, <harvestcraft:cantaloupeItem>,
    <harvestcraft:cantaloupeItem>, <harvestcraft:cantaloupeItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:kiwiseedItem>);

// Raspberry
recipes.remove(<harvestcraft:raspberryItem>);
recipes.addShapeless(<harvestcraft:raspberryItem>, [
    <harvestcraft:strawberryItem>, <harvestcraft:strawberryItem>,
    <harvestcraft:strawberryItem>, <harvestcraft:strawberryItem>,
    <minecraft:red_flower:4>, <minecraft:red_flower:4>,
    <minecraft:red_flower:4>, <minecraft:red_flower:4>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:raspberryseedItem>);

// Blueberry
recipes.remove(<harvestcraft:blueberryItem>);
recipes.addShapeless(<harvestcraft:blueberryItem>, [
    <harvestcraft:strawberryItem>, <harvestcraft:strawberryItem>,
    <harvestcraft:strawberryItem>, <harvestcraft:strawberryItem>,
    <minecraft:red_flower:1>, <minecraft:red_flower:1>,
    <minecraft:red_flower:1>, <minecraft:red_flower:1>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:blueberryseedItem>);

// Blackberry
recipes.remove(<harvestcraft:blackberryItem>);
recipes.addShapeless(<harvestcraft:blackberryItem>, [
    <harvestcraft:strawberryItem>, <harvestcraft:strawberryItem>,
    <harvestcraft:strawberryItem>, <harvestcraft:strawberryItem>,
    <harvestcraft:blueberryItem>, <harvestcraft:blueberryItem>,
    <harvestcraft:blueberryItem>, <harvestcraft:blueberryItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:blackberryseedItem>);

// Coffee Beans
recipes.remove(<harvestcraft:coffeebeanItem>);
recipes.addShapeless(<harvestcraft:coffeebeanItem>, [
    <harvestcraft:beanItem>, <harvestcraft:beanItem>,
    <harvestcraft:beanItem>, <harvestcraft:beanItem>,
    <minecraft:reeds>, <minecraft:reeds>,
    <minecraft:reeds>, <minecraft:reeds>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:coffeeseedItem>);

// Asparagus
recipes.remove(<harvestcraft:asparagusItem>);
recipes.addShapeless(<harvestcraft:asparagusItem>, [
    <harvestcraft:scallionItem>, <harvestcraft:scallionItem>,
    <harvestcraft:scallionItem>, <harvestcraft:scallionItem>,
    <harvestcraft:cornItem>, <harvestcraft:cornItem>,
    <harvestcraft:cornItem>, <harvestcraft:cornItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:asparagusseedItem>);

// Cauliflower
recipes.remove(<harvestcraft:cauliflowerItem>);
recipes.addShapeless(<harvestcraft:cauliflowerItem>, [
    <harvestcraft:cabbageItem>, <harvestcraft:cabbageItem>,
    <harvestcraft:cabbageItem>, <harvestcraft:cabbageItem>,
    <harvestcraft:lettuceItem>, <harvestcraft:lettuceItem>,
    <harvestcraft:lettuceItem>, <harvestcraft:lettuceItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:cauliflowerseedItem>);

// Grape
recipes.remove(<harvestcraft:grapeItem>);
recipes.addShapeless(<harvestcraft:grapeItem>, [
    <harvestcraft:blueberryItem>, <harvestcraft:blueberryItem>,
    <harvestcraft:blueberryItem>, <harvestcraft:blueberryItem>,
    <harvestcraft:cantaloupeItem>, <harvestcraft:cantaloupeItem>,
    <harvestcraft:cantaloupeItem>, <harvestcraft:cantaloupeItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:grapeseedItem>);

// Cranberry
recipes.remove(<harvestcraft:cranberryItem>);
recipes.addShapeless(<harvestcraft:cranberryItem>, [
    <harvestcraft:blueberryItem>, <harvestcraft:blueberryItem>,
    <harvestcraft:blueberryItem>, <harvestcraft:blueberryItem>,
    <harvestcraft:grapeItem>, <harvestcraft:grapeItem>,
    <harvestcraft:grapeItem>, <harvestcraft:grapeItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:cranberryseedItem>);

// Onion
recipes.remove(<harvestcraft:onionItem>);
recipes.addShapeless(<harvestcraft:onionItem>, [
    <harvestcraft:celeryItem>, <harvestcraft:celeryItem>,
    <harvestcraft:celeryItem>, <harvestcraft:celeryItem>,
    <harvestcraft:brusselsproutItem>, <harvestcraft:brusselsproutItem>,
    <harvestcraft:brusselsproutItem>, <harvestcraft:brusselsproutItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:onionseedItem>);

// Chili Pepper
recipes.remove(<harvestcraft:chilipepperItem>);
recipes.addShapeless(<harvestcraft:chilipepperItem>, [
    <harvestcraft:tomatoItem>, <harvestcraft:tomatoItem>,
    <harvestcraft:tomatoItem>, <harvestcraft:tomatoItem>,
    <harvestcraft:onionItem>, <harvestcraft:onionItem>,
    <harvestcraft:onionItem>, <harvestcraft:onionItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:chilipepperseedItem>);

// Mustard Seeds
recipes.remove(<harvestcraft:mustardseedsItem>);
recipes.addShapeless(<harvestcraft:mustardseedsItem>, [
    <harvestcraft:chilipepperItem>, <harvestcraft:chilipepperItem>,
    <harvestcraft:chilipepperItem>, <harvestcraft:chilipepperItem>,
    <harvestcraft:beanItem>, <harvestcraft:beanItem>,
    <harvestcraft:beanItem>, <harvestcraft:beanItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:mustardseedItem>);

// Bamboo Shoot
recipes.remove(<harvestcraft:bambooshootItem>);
recipes.addShapeless(<harvestcraft:bambooshootItem>, [
    <harvestcraft:cornItem>, <harvestcraft:cornItem>,
    <harvestcraft:cornItem>, <harvestcraft:cornItem>,
    <harvestcraft:riceItem>, <harvestcraft:riceItem>,
    <harvestcraft:riceItem>, <harvestcraft:riceItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:bambooshootseedItem>);

// Peanut
recipes.remove(<harvestcraft:peanutItem>);
recipes.addShapeless(<harvestcraft:peanutItem>, [
    <harvestcraft:peasItem>, <harvestcraft:peasItem>,
    <harvestcraft:peasItem>, <harvestcraft:peasItem>,
    <harvestcraft:bambooshootItem>, <harvestcraft:bambooshootItem>,
    <harvestcraft:bambooshootItem>, <harvestcraft:bambooshootItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:peanutseedItem>);

// Ginger
recipes.remove(<harvestcraft:gingerItem>);
recipes.addShapeless(<harvestcraft:gingerItem>, [
    <harvestcraft:mustardseedsItem>, <harvestcraft:mustardseedsItem>,
    <harvestcraft:mustardseedsItem>, <harvestcraft:mustardseedsItem>,
    <harvestcraft:peanutItem>, <harvestcraft:peanutItem>,
    <harvestcraft:peanutItem>, <harvestcraft:peanutItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:gingerseedItem>);

// Garlic
recipes.remove(<harvestcraft:garlicItem>);
recipes.addShapeless(<harvestcraft:garlicItem>, [
    <harvestcraft:onionItem>, <harvestcraft:onionItem>,
    <harvestcraft:onionItem>, <harvestcraft:onionItem>,
    <harvestcraft:gingerItem>, <harvestcraft:gingerItem>,
    <harvestcraft:gingerItem>, <harvestcraft:gingerItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:garlicseedItem>);

// White Mushroom
recipes.remove(<harvestcraft:whitemushroomItem>);
recipes.addShapeless(<harvestcraft:whitemushroomItem>, [
    <minecraft:brown_mushroom>, <minecraft:brown_mushroom>,
    <minecraft:brown_mushroom>, <minecraft:brown_mushroom>,
    <minecraft:red_mushroom>, <minecraft:red_mushroom>,
    <minecraft:red_mushroom>, <minecraft:red_mushroom>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:whitemushroomseedItem>);

// Sesame Seeds
recipes.remove(<harvestcraft:sesameseedsItem>);
recipes.addShapeless(<harvestcraft:sesameseedsItem>, [
    <harvestcraft:riceItem>, <harvestcraft:riceItem>,
    <harvestcraft:riceItem>, <harvestcraft:riceItem>,
    <harvestcraft:coffeebeanItem>, <harvestcraft:coffeebeanItem>,
    <harvestcraft:coffeebeanItem>, <harvestcraft:coffeebeanItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:sesameseedsseedItem>);

// Seaweed
recipes.remove(<harvestcraft:seaweedItem>);
recipes.addShapeless(<harvestcraft:seaweedItem>, [
    <harvestcraft:lettuceItem>, <harvestcraft:lettuceItem>,
    <harvestcraft:lettuceItem>, <harvestcraft:lettuceItem>,
    <harvestcraft:celeryItem>, <harvestcraft:celeryItem>,
    <harvestcraft:celeryItem>, <harvestcraft:celeryItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:seaweedseedItem>);

// Tea Leaf
recipes.remove(<harvestcraft:tealeafItem>);
recipes.addShapeless(<harvestcraft:tealeafItem>, [
    <harvestcraft:seaweedItem>, <harvestcraft:seaweedItem>,
    <harvestcraft:seaweedItem>, <harvestcraft:seaweedItem>,
    <harvestcraft:soybeanItem>, <harvestcraft:soybeanItem>,
    <harvestcraft:soybeanItem>, <harvestcraft:soybeanItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:teaseedItem>);

// Spice Leaf
recipes.remove(<harvestcraft:spiceleafItem>);
recipes.addShapeless(<harvestcraft:spiceleafItem>, [
    <harvestcraft:tealeafItem>, <harvestcraft:tealeafItem>,
    <harvestcraft:tealeafItem>, <harvestcraft:tealeafItem>,
    <harvestcraft:chilipepperItem>, <harvestcraft:chilipepperItem>,
    <harvestcraft:chilipepperItem>, <harvestcraft:chilipepperItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:spiceleafseedItem>);

// Parsnip
recipes.remove(<harvestcraft:parsnipItem>);
recipes.addShapeless(<harvestcraft:parsnipItem>, [
    <minecraft:carrot>, <minecraft:carrot>,
    <minecraft:carrot>, <minecraft:carrot>,
    <harvestcraft:beetItem>, <harvestcraft:beetItem>,
    <harvestcraft:beetItem>, <harvestcraft:beetItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:parsnipseedItem>);

// Pineapple
recipes.remove(<harvestcraft:pineappleItem>);
recipes.addShapeless(<harvestcraft:pineappleItem>, [
    <harvestcraft:bambooshootItem>, <harvestcraft:bambooshootItem>,
    <harvestcraft:bambooshootItem>, <harvestcraft:bambooshootItem>,
    <harvestcraft:cantaloupeItem>, <harvestcraft:cantaloupeItem>,
    <harvestcraft:cantaloupeItem>, <harvestcraft:cantaloupeItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:pineappleseedItem>);

// Turnip
recipes.remove(<harvestcraft:turnipItem>);
recipes.addShapeless(<harvestcraft:turnipItem>, [
    <harvestcraft:parsnipItem>, <harvestcraft:parsnipItem>,
    <harvestcraft:parsnipItem>, <harvestcraft:parsnipItem>,
    <harvestcraft:radishItem>, <harvestcraft:radishItem>,
    <harvestcraft:radishItem>, <harvestcraft:radishItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:turnipseedItem>);

// Rutabaga
recipes.remove(<harvestcraft:rutabagaItem>);
recipes.addShapeless(<harvestcraft:rutabagaItem>, [
    <harvestcraft:beetItem>, <harvestcraft:beetItem>,
    <harvestcraft:beetItem>, <harvestcraft:beetItem>,
    <harvestcraft:turnipItem>, <harvestcraft:turnipItem>,
    <harvestcraft:turnipItem>, <harvestcraft:turnipItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:rutabagaseedItem>);

// Sunflower Seeds
recipes.remove(<harvestcraft:sunflowerseedsItem>);
recipes.addShapeless(<harvestcraft:sunflowerseedsItem>, [
    <minecraft:wheat_seeds>, <minecraft:wheat_seeds>,
    <minecraft:wheat_seeds>, <minecraft:wheat_seeds>,
    <minecraft:yellow_flower>, <minecraft:yellow_flower>,
    <minecraft:yellow_flower>, <minecraft:yellow_flower>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
#recipes.remove(<harvestcraft:sunflowerseedItem>);

// Bellpepper
recipes.remove(<harvestcraft:bellpepperItem>);
recipes.addShapeless(<harvestcraft:bellpepperItem>, [
    <harvestcraft:chilipepperItem>, <harvestcraft:chilipepperItem>,
    <harvestcraft:chilipepperItem>, <harvestcraft:chilipepperItem>,
    <harvestcraft:spiceleafItem>, <harvestcraft:spiceleafItem>,
    <harvestcraft:spiceleafItem>, <harvestcraft:spiceleafItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:bellpepperseedItem>);

// Cotton
recipes.remove(<harvestcraft:cottonItem>);
recipes.addShapeless(<harvestcraft:cottonItem>, [
    <harvestcraft:soybeanItem>, <harvestcraft:soybeanItem>,
    <harvestcraft:soybeanItem>, <harvestcraft:soybeanItem>,
    <harvestcraft:barleyItem>, <harvestcraft:barleyItem>,
    <harvestcraft:barleyItem>, <harvestcraft:barleyItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:cottonseedItem>);

// Cactus Fruit
recipes.remove(<harvestcraft:cactusfruitItem>);
recipes.addShapeless(<harvestcraft:cactusfruitItem>, [
    <harvestcraft:kiwiItem>, <harvestcraft:kiwiItem>,
    <harvestcraft:kiwiItem>, <harvestcraft:kiwiItem>,
    <harvestcraft:bambooshootItem>, <harvestcraft:bambooshootItem>,
    <harvestcraft:bambooshootItem>, <harvestcraft:bambooshootItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:cactusfruitseedItem>);

// Candleberry
recipes.remove(<harvestcraft:candleberryItem>);
recipes.addShapeless(<harvestcraft:candleberryItem>, [
    <harvestcraft:cactusfruitItem>, <harvestcraft:cactusfruitItem>,
    <harvestcraft:cactusfruitItem>, <harvestcraft:cactusfruitItem>,
    <harvestcraft:grapeItem>, <harvestcraft:grapeItem>,
    <harvestcraft:grapeItem>, <harvestcraft:grapeItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:candleberryseedItem>);

// Curry Leaf
recipes.remove(<harvestcraft:curryleafItem>);
recipes.addShapeless(<harvestcraft:curryleafItem>, [
    <harvestcraft:spiceleafItem>, <harvestcraft:spiceleafItem>,
    <harvestcraft:spiceleafItem>, <harvestcraft:spiceleafItem>,
    <harvestcraft:mustardseedsItem>, <harvestcraft:mustardseedsItem>,
    <harvestcraft:mustardseedsItem>, <harvestcraft:mustardseedsItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:curryleafseedItem>);

// Spinach
recipes.remove(<harvestcraft:spinachItem>);
recipes.addShapeless(<harvestcraft:spinachItem>, [
    <minecraft:cactus>, <minecraft:cactus>,
    <minecraft:cactus>, <minecraft:cactus>,
    <harvestcraft:lettuceItem>, <harvestcraft:lettuceItem>,
    <harvestcraft:lettuceItem>, <harvestcraft:lettuceItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:spinachseedItem>);

// Cucumber
recipes.remove(<harvestcraft:cucumberItem>);
recipes.addShapeless(<harvestcraft:cucumberItem>, [
    <harvestcraft:peasItem>, <harvestcraft:peasItem>,
    <harvestcraft:peasItem>, <harvestcraft:peasItem>,
    <harvestcraft:okraItem>, <harvestcraft:okraItem>,
    <harvestcraft:okraItem>, <harvestcraft:okraItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:cucumberseedItem>);

// Artichoke
recipes.remove(<harvestcraft:artichokeItem>);
recipes.addShapeless(<harvestcraft:artichokeItem>, [
    <harvestcraft:asparagusItem>, <harvestcraft:asparagusItem>,
    <harvestcraft:asparagusItem>, <harvestcraft:asparagusItem>,
    <harvestcraft:lettuceItem>, <harvestcraft:lettuceItem>,
    <harvestcraft:lettuceItem>, <harvestcraft:lettuceItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:artichokeseedItem>);

// Zucchini
recipes.remove(<harvestcraft:zucchiniItem>);
recipes.addShapeless(<harvestcraft:zucchiniItem>, [
    <minecraft:pumpkin>, <minecraft:pumpkin>,
    <minecraft:pumpkin>, <minecraft:pumpkin>,
    <harvestcraft:cucumberItem>, <harvestcraft:cucumberItem>,
    <harvestcraft:cucumberItem>, <harvestcraft:cucumberItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:zucchiniseedItem>);

// Eggplant
recipes.remove(<harvestcraft:eggplantItem>);
recipes.addShapeless(<harvestcraft:eggplantItem>, [
    <harvestcraft:zucchiniItem>, <harvestcraft:zucchiniItem>,
    <harvestcraft:zucchiniItem>, <harvestcraft:zucchiniItem>,
    <harvestcraft:tomatoItem>, <harvestcraft:tomatoItem>,
    <harvestcraft:tomatoItem>, <harvestcraft:tomatoItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:eggplantseedItem>);

// Rhubarb
recipes.remove(<harvestcraft:rhubarbItem>);
recipes.addShapeless(<harvestcraft:rhubarbItem>, [
    <minecraft:reeds>, <minecraft:reeds>,
    <minecraft:reeds>, <minecraft:reeds>,
    <harvestcraft:lettuceItem>, <harvestcraft:lettuceItem>,
    <harvestcraft:lettuceItem>, <harvestcraft:lettuceItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:rhubarbseedItem>);

// Winter Squash
recipes.remove(<harvestcraft:wintersquashItem>);
recipes.addShapeless(<harvestcraft:wintersquashItem>, [
    <minecraft:pumpkin>, <minecraft:pumpkin>,
    <minecraft:pumpkin>, <minecraft:pumpkin>,
    <harvestcraft:zucchiniItem>, <harvestcraft:zucchiniItem>,
    <harvestcraft:zucchiniItem>, <harvestcraft:zucchiniItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:wintersquashseedItem>);

// Water Chestnut
recipes.remove(<harvestcraft:waterchestnutItem>);
recipes.addShapeless(<harvestcraft:waterchestnutItem>, [
    <harvestcraft:sesameseedsItem>, <harvestcraft:sesameseedsItem>,
    <harvestcraft:sesameseedsItem>, <harvestcraft:sesameseedsItem>,
    <harvestcraft:seaweedItem>, <harvestcraft:seaweedItem>,
    <harvestcraft:seaweedItem>, <harvestcraft:seaweedItem>,
    <projecte:item.pe_philosophers_stone>.reuse()]);
recipes.remove(<harvestcraft:waterchestnutseedItem>);

print("Initialized 'HarvestCraft plant recipes'");