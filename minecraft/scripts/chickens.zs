#Name: chickens.zs
#Author: cryxli

print("Initializing 'Chickens recipes'...");

# flint chicken
recipes.remove(<chickens:spawn_egg:101>);
recipes.addShaped(<chickens:spawn_egg:101>, [[<minecraft:flint>, <minecraft:flint>, <minecraft:flint>], [<minecraft:flint>, <minecraft:egg>, <minecraft:flint>], [<minecraft:flint>, <minecraft:flint>, <minecraft:flint>]]);

# sand chicken
recipes.remove(<chickens:spawn_egg:105>);
recipes.addShaped(<chickens:spawn_egg:105>, [[<ore:sand>, <ore:sand>, <ore:sand>], [<ore:sand>, <minecraft:egg>, <ore:sand>], [<ore:sand>, <ore:sand>, <ore:sand>]]);

# log chicken
recipes.remove(<chickens:spawn_egg:108>);
recipes.addShaped(<chickens:spawn_egg:108>, [[<ore:logWood>, <ore:logWood>, <ore:logWood>], [<ore:logWood>, <minecraft:egg>, <ore:logWood>], [<ore:logWood>, <ore:logWood>, <ore:logWood>]]);

print("Initialized 'Chickens recipes'");