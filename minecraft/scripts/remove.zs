#Name: remove.zs
#Author: cryxli

print("Initializing 'remove'...");

# remove harvestcraft items that give plants and fruits
recipes.remove(<harvestcraft:market>);
recipes.remove(<harvestcraft:shippingbin>);

print("Initialized 'remove'");