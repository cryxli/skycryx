#!/usr/bin/python

import sys
import urllib2
import json
import re
import sqlite3


def connect_to_database():
    """
    Open connection to SQLite3 database and ensure that the tables exist.
    """
    conn = sqlite3.connect('curse.sqlite3')
    cur = conn.cursor()

    cur.execute("SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name='CMF_PROJECT'")
    if 0 == cur.fetchone()[0]:
        cur.execute("CREATE TABLE CMF_PROJECT (PRJ_ID INTEGER PRIMARY KEY, PRJ_TITLE TEXT, PRJ_DESC TEXT, PRJ_URL TEXT, PRJ_LOGO TEXT, PRJ_FILES TEXT)")
        print "Table CMF_PROJECT created"


    cur.execute("SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name='CMF_FILE'")
    if 0 == cur.fetchone()[0]:
        cur.execute("CREATE TABLE CMF_FILE(FIL_HASH TEXT NOT NULL, FIL_PRJ_ID INTEGER NOT NULL, FIL_ID INTEGER NOT NULL, FIL_TYPE TEXT, FIL_NAME TEXT)")
        print "Table CMF_FILE created"

    return conn

def download_file(url):
    """
    Download the given resource by its URL without throwing exceptions.

    Returns the content of the resource as text, or, None, if downloading
    failed.
    """
    try:
        print "Downloading resource %s" % url
        return urllib2.urlopen(url).read()
    except urllib2.HTTPError as e:
        print "  Failed to download: code = %s" % e.code
    except urllib2.URLError as e:
        print "  Failed to download: %s" % e.reason
    return None;

def download_project_info(project, cur = None):
    """
    Get the project information from its page on curse and update the database.

    Won't do anything, if the project page cannot be loaded, or, the 
    information cannot be parsed completely.
    """
    if cur is None:
        return None

    url = "https://minecraft.curseforge.com/projects/%s" % project
    text = download_file(url)
    if text is None:
        return None

    r = re.compile('<meta property="og:title" content="([^"]+)"')
    m = r.search(text)
    if m is None:
        return None
    title = m.group(1)

    r = re.compile('<meta property="og:description" content="([^"]+)"')
    m = r.search(text)
    if m is None:
        return None
    desc = m.group(1)

    r = re.compile('<meta property="og:url" content="([^"]+)"')
    m = r.search(text)
    if m is None:
        return None
    url = m.group(1)

    r = re.compile('<meta property="og:image" content="([^"]+)"')
    m = r.search(text)
    if m is None:
        return None
    logo = m.group(1)

    r = re.compile('<a\s+href="([^"]+)"\s*>\s*Files\s*</a>')
    m = r.search(text)
    if m is None:
        return None
    files = "https://minecraft.curseforge.com%s" % m.group(1)

    r = re.compile('<a\s+href="https://mods.curse.com/project/(\d+)"\s*>\s*View on Curse.com\s*</a>')
    m = r.search(text)
    if m is None:
        return None
    project_id = long(m.group(1))

    if cur.execute("SELECT COUNT(*) FROM CMF_PROJECT WHERE PRJ_ID = ?", (project_id,)).fetchone()[0] == 0:
        cur.execute("INSERT INTO CMF_PROJECT VALUES (?,?,?,?,?,?)", (project_id, title, desc, url, logo, files))
    else:
        cur.execute("UPDATE CMF_PROJECT SET PRJ_TITLE = ?, PRJ_DESC = ?, PRJ_URL = ?, PRJ_LOGO = ?, PRJ_FILES = ? WHERE PRJ_ID = ?", (title, desc, url, logo, files, project_id))
    return (project_id, title, desc, url, logo, files)


def check_file_versions(project_info, cur = None):
    """
    Download information about the latest (first page) files of the given
    project and update the database.

    Won't do anything, if the files page cannot be loaded, or, the release
    information cannot be parsed completely.
    """
    if cur is None:
        return

    text = download_file(project_info[5])
    if text is None:
        return

    r = re.compile('<tr\s+class="project-file-list-item"\s*>\s*(.+?)\s*</tr>', re.DOTALL)
    for row in r.findall(text):
        r = re.compile('/files/(\d+)/download')
        m = r.search(row)
        if m is None:
            continue
        file_id = long(m.group(1))

        if cur.execute("SELECT COUNT(*) FROM CMF_FILE WHERE FIL_PRJ_ID = ? AND FIL_ID = ?", (project_info[0],file_id)).fetchone()[0] > 0:
            continue

        r = re.compile('class="project-file-release-type".+?title="([^"]+)"', re.DOTALL)
        m = r.search(row)
        if m is None:
            continue
        type = m.group(1).lower()

        r = re.compile('class="project-file-name-container".+?>([^<]+)</a>', re.DOTALL)
        m = r.search(row)
        if m is None:
            continue
        file_name = m.group(1)

        hash = get_file_hash(project_info, file_id)
        if hash is None:
            continue

        cur.execute("INSERT INTO CMF_FILE VALUES (?,?,?,?,?)", (hash, project_info[0], file_id, type, file_name))


def get_file_hash(project_info, file_id):
    """
    Get the MD5 hash of the given file (by ID) from the given project.

    Will return None, if the page could not be downloaded, or, the MD5 hash
    could not be extracted.
    """
    text = download_file("%s/%d" %(project_info[5], file_id))
    if text is None:
        return None

    r = re.compile('<span class="md5">([^<]+)</span')
    m = r.search(text)
    if not m is None:
        return m.group(1).lower()
    else:
        return None

def get_project_info(p, cur = None, force_update_projects = False):
    if cur is None:
        return None
    project_info = None
    # check whether that project already exists
    if force_update_projects or cur.execute("SELECT COUNT(*) FROM CMF_PROJECT WHERE PRJ_URL LIKE ?", ("%%/%s" % p,)).fetchone()[0] == 0:
        # project not found, try to download
        project_info = download_project_info(p, cur)
        if project_info is None:
            # failed to download
            print "Project %s not found" % p
        else:
            # added to database
            print "Project %s downloaded" % project_info[1]
    else:
        # already exists
        project_info = cur.execute("SELECT * FROM CMF_PROJECT WHERE PRJ_URL LIKE ?", ("%%/%s" % p,)).fetchone()
        print "Project %s found" % project_info[1]
    return project_info


def add_project(p_list = []):
    if not type(p_list) in [list,tuple] or len(p_list) == 0:
        return
    conn = connect_to_database()
    cur = conn.cursor()
    for p in p_list:
        get_project_info(p, cur)
    conn.commit()
    conn.close()


def update_files(p_list = []):
    if not type(p_list) in [list,tuple] or len(p_list) == 0:
        return
    conn = connect_to_database()
    cur = conn.cursor()
    for p in p_list:
        project_info = get_project_info(p, cur)
        if project_info is None:
            continue
        print "Looking for files for %s" % project_info[1]
        check_file_versions(project_info, cur)
        conn.commit();
    conn.commit()
    conn.close()


def update_all_files():
    conn = connect_to_database()
    cur = conn.cursor()

    # check file versions for each entry in the projects table
    projects = cur.execute("SELECT * FROM CMF_PROJECT ORDER BY PRJ_URL").fetchall()
    for project_info in projects:
        print "Looking for files for %s" % project_info[1]
        check_file_versions(project_info, cur)
        conn.commit();

    conn.commit()
    conn.close()


def print_help(script = "curse.py"):
    print "Usage: %s <option>" % script
    print
    print "Possible options are:"
    print "  add <project>"
    print "    Add the project to the database. This command does not download any"
    print "    file verions."
    print
    print "  update <project>"
    print "    Update file version of the given project. If the project does not"
    print "    exist in the database, it will be created. If the project cannot be"
    print "    found, nothing is updated."
    print
    print "  update all"
    print "    Update file versions of all projects in the database."
    print


def analyse_arguments(args):
    if len(args) < 2:
        print_help(args[0])
        return
    s = args[1].lower()
    if "add" == s and len(args) > 2:
        add_project(args[2:])
    elif "update" == s and len(args) == 3 and args[2].lower() == "all":
        update_all_files()
    elif "update" == s and len(args) > 2:
        update_files(args[2:])
    else:
        print_help(args[0])


if __name__ == '__main__':
    analyse_arguments(sys.argv)
