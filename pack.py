#!/usr/bin/python

"""
This python script is intended for MultiMC users that want to automate modpack
creation from their current MultiMC instance.

Put this script in the root of your MultiMC instance and run it.
TODO
"""

import os
import json
from collections import OrderedDict
import time
import hashlib
import subprocess
from zipfile import ZipFile, ZIP_DEFLATED
import sqlite3

# settings for manifest.json
pack_name = "SkyCryx"
pack_version = "0.1.0"
pack_author = "cryxli"


def md5sum(filename, blocksize=65536):
    """
    Calculate the MD5 hash of the given file.
    """
    hash = hashlib.md5()
    with open(filename, "rb") as f:
        for block in iter(lambda: f.read(blocksize), b""):
            hash.update(block)
    return hash.hexdigest()


def multi_mc_config(manifest = None):
    """
    Read Forge version from MultiMC's patches/net.minecraftforge.json file.
    """
    print "Analysing MultiMC config"
    if manifest is None:
        manifest = OrderedDict()
    # reading MC and forge version from MultiMC
    with open('patches/net.minecraftforge.json') as data_file:
        data = json.load(data_file)
        manifest["minecraft"] = OrderedDict()
        manifest["minecraft"]["version"] = data["mcVersion"],
        manifest["minecraft"]["modLoaders"] = []
        forge = OrderedDict()
        forge["id"] = "forge%s" % data["version"][len(data["mcVersion"]):]
        forge["primary"] = True
        manifest["minecraft"]["modLoaders"].append(forge)
    print "  Minecraft version: %s" % manifest["minecraft"]["version"]
    print "  Forge version....: %s" % manifest["minecraft"]["modLoaders"][0]["id"]
    # for chaining
    return manifest


def get_mod_candidates():
    """
    Scan the mods folder for possible mod JARs and calculate the MD5 sum of that
    file.

    Returns a list of tuples containing the filename and the md5 hash.
    """
    print "Looking for mods"
    mods = []
    for f in os.listdir("./minecraft/mods"):
        filepath = os.path.join("./minecraft/mods", f)
        if os.path.isfile(filepath):
            if f[-4:].lower() == ".jar" or f[-4:].lower() == ".zip":
                hash = md5sum(filepath).lower()
                print "  Candidate:"
                print "    %s" % f
                print "    hash = %s" % hash
                mods.append((f, hash))
    return mods


# @unused
def load_mod_csv(filename, fileIds):
    """
    Loading CSV files with MD5 hash and assigned PEROJECT_ID and FILE_ID. The
    capital strings are CSV headers.

    No longer used in favour of the SQLite databse.
    """
    f = open(filename, 'r')
    line = f.readline().strip()
    # guess soulmn separator
    separator = ","
    if len(line.split(separator)) < 3:
        separator = ";"
        if len(line.split(separator)) < 3:
            separator = "\t"
    header = line.upper().split(separator)
    idx_hash = header.index("MD5")
    idx_project = header.index("PEROJECT_ID")
    idx_file = header.index("FILE_ID")
    # read data
    while line > "":
        cols = line.split(separator)
        fileIds[cols[idx_hash].lower()] = (cols[idx_project], cols[idx_file])
        line = f.readline().strip()
    f.close()
    return fileIds

# @unused
def resolve_mod_versions(manifest, mods):
    """
    Load all sources of hash and IDs then add known MOD files to the manifest.
    The MODs that cannot be resolfed will be treated as modpack resources.

    CSV Sources in order of reading:
    - ./minecraft/mods/mods.csv
    - ./minecraft/mods/mods.txt
    - ./mods.csv
    - ./mods.txt

    Entries in later files will replace entries in earler loaded files.

    No longer used in favour of the SQLite databse.
    """
    print "Resolving mod versions"
    # load JAR hashes and project and file IDs
    fileIds = {}
    csvsrc = ["./minecraft/mods/mods.csv", "./minecraft/mods/mods.txt", "./mods.csv", "./mods.txt"]
    for f in csvsrc:
        if os.path.isfile(f):
            print "  Loading %s" % f
            load_mod_csv(f, fileIds)
    # add known file IDs to manifest from mod list
    unknwon_mods = []
    for (filename, md5) in mods:
        if not md5 in fileIds:
            unknwon_mods.append(filename)
            print "  Unknown mod:"
            print "    %s" % filename
            print "    hash = %s" % md5
        else:
            (projectId, fileId) = fileIds[md5]
            entry = OrderedDict()
            entry["projectID"] = projectId
            entry["fileID"] = fileId
            entry["required"] = True
            manifest["files"].append(entry)
    # return list of JARs that could not be resolved
    return unknwon_mods

def lookup_mod_versions(manifest, mods):
    """
    Look up project and file IDs given the MD5 hashes of the mod files from a
    SQLite database. Look at curse.py for a tool to create and maintain that
    database.

    Resolved files will be added to the manifest. Unknown mods will be returned
    by their file name.
    """
    conn = sqlite3.connect('curse.sqlite3')
    cur = conn.cursor()

    unknwon_mods = []
    for (filename, md5) in mods:
        if cur.execute("SELECT COUNT(*) FROM CMF_FILE WHERE FIL_HASH = ?", (md5,)).fetchone()[0] == 1:
            # hash found
            mod_info = cur.execute("SELECT FIL_PRJ_ID, FIL_ID FROM CMF_FILE WHERE FIL_HASH = ?", (md5,)).fetchone()
            entry = OrderedDict()
            entry["projectID"] = mod_info[0]
            entry["fileID"] = mod_info[1]
            entry["required"] = True
            manifest["files"].append(entry)
        else:
            # unknown hash
            unknwon_mods.append(filename)
            print "  Unknown mod:"
            print "    %s" % filename
            print "    hash = %s" % md5

    conn.close()
    return unknwon_mods


def write_manifest(manifest):
    """
    Write a pretty printed version of the manifest to disk.
    """
    print "Writing manifest.json"
    f = open("./minecraft/manifest.json", "w")
    f.write(json.dumps(manifest, indent = 2))
    f.close()

def read_git_filelist():
    """
    Get the list of currently versioned files from git. Remove the files that
    are not related to the modpack directly but used to organise the project.
    """
    print "Analyse git history"
    p = subprocess.Popen("git ls-tree -r master --name-only", shell=True, stdout=subprocess.PIPE)
    pack_files = []
    for line in p.stdout.readlines():
        path = line.strip()
        if path.startswith("minecraft/"):
            pack_files.append(path)
    p.wait()
    # remove none pack files
    if pack_files.count("minecraft/manifest.json") > 0:
        pack_files.remove("minecraft/manifest.json")
    if pack_files.count("minecraft/options.txt") > 0:
        pack_files.remove("minecraft/options.txt")
    print "  Found %d files" % len(pack_files)
    # return remaining list
    return pack_files

def write_zip(pack_files, mods = [], zip_prefix = ""):
    """
    Create a ZIP archive from all the crate and analysed files that should be
    compatible with Curse.com's modpack policy.
    """
    zipfilename = zip_prefix + time.strftime("-%Y%m%d-%H%M%S.zip", time.gmtime())
    print "Creating %s" % zipfilename
    zip = ZipFile(zipfilename, "w", ZIP_DEFLATED)
    print "  Adding manifest.json"
    zip.write("./minecraft/manifest.json", "manifest.json")
    # versioned files
    for path in pack_files:
        arcname = overrides + path[9:]
        print "  Adding %s" % path
        zip.write(path, arcname)
        print "    as %s" % arcname
    # unknown mods
    for path in mods:
        src = "./minecraft/mods/%s" % path
        arcname = "%s/mods/%s" % (overrides, path)
        print "  Adding mod %s" % src
        zip.write(src, arcname)
        print "    as %s" % arcname
    # done
    zip.close()

# analyse MultiMC config
manifest = multi_mc_config()

# adding some default values to manifest
overrides = "overrides"
manifest["manifestType"] = "minecraftModpack"
manifest["manifestVersion"] = 1
manifest["name"] = pack_name
manifest["version"] = pack_version
manifest["author"] = pack_author
manifest["files"] = []
manifest["overrides"] = overrides

# looking for mods
mods = get_mod_candidates()

# resolve mod versions
#mods = resolve_mod_versions(manifest, mods)
mods = lookup_mod_versions(manifest, mods)

# writing manifest.json
write_manifest(manifest)
manifest = None

# Analyse git history
pack_files = read_git_filelist()

# create ZIP file
zip_prefix = "%s-%s" % (pack_name, pack_version)
write_zip(pack_files, mods, zip_prefix)

print
print "All done."
if len(mods) > 0:
    print "WARNING: There are unknown mods that have been added to the %s directory within the zip" % overrides
for mod in mods:
    print "  - %s" % mod
print
